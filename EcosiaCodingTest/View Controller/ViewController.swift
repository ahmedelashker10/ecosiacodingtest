//
//  ViewController.swift
//  EcosiaCodingTest
//
//  Created by Ahmed Elashker on 3/29/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UITableViewDataSource {
    
    //MARK: Outlets
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var audioSlider: UISlider!
    @IBOutlet weak var lblElapsed: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var tableViewMetadata: UITableView!
    
    //MARK: Variables
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()
    var audioList = [Constants.kMP3CommDemo5,
                     Constants.kMP3CommDemo7,
                     Constants.kMP3CommDemo12,
                     Constants.kMP3Grimey,
                     Constants.kMP3Triumph]
    var audioIndex = 0
    var audioTimer = Timer()
    var audioMetadata:[AVMetadataItem] = []
    
    //MARK: Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        roundButtons()
        loadMetaData()
        initAudioPlayer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func roundButtons() {
        btnPlay.layer.cornerRadius = btnPlay.frame.height / 2
        btnForward.layer.cornerRadius = btnForward.frame.height / 2
        btnBackward.layer.cornerRadius = btnBackward.frame.height / 2
    }
    
    func loadMetaData() {
        let playerItem = AVPlayerItem(url: getCurrentAudioURL())
        audioMetadata = playerItem.asset.commonMetadata
        tableViewMetadata.reloadData()
    }
    
    func initAudioPlayer()  {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            try audioPlayer = AVAudioPlayer(contentsOf: getCurrentAudioURL())
            audioSlider.value = 0
            audioSlider.maximumValue = Float(audioPlayer.duration)
            lblElapsed.text = Helpers.calculateTimeInterval(seconds: Int(audioSlider.value))
            lblDuration.text = Helpers.calculateTimeInterval(seconds: Int(audioPlayer.duration))
        }
        catch {
            print(error)
        }
        
        if audioList.index(of: audioList.last!) == audioIndex {
            disableButton(btn: btnForward)
        }
        else {
            enableButton(btn: btnForward)
        }
        
        if audioList.index(of: audioList.first!) == audioIndex {
            disableButton(btn: btnBackward)
        }
        else {
            enableButton(btn: btnBackward)
        }
    }
    
    func getCurrentAudioURL() -> URL {
        let audioPath = Bundle.main.path(forResource: audioList[audioIndex], ofType: Constants.kTypeMP3)
        let audioURL = URL(fileURLWithPath: audioPath!)
        return audioURL
    }
    
    func disableButton(btn:UIButton) {
        btn.isUserInteractionEnabled = false
        btn.alpha = 0.3
    }
    
    func enableButton(btn:UIButton) {
        btn.isUserInteractionEnabled = true
        btn.alpha = 1.0
    }
    
    //MARK: Actions
    @IBAction func toggleAudio(_ sender: Any) {
        if !audioPlayer.isPlaying {
            playAudio()
        }
        else {
            pauseAudio()
        }
    }
    
    func playAudio() {
        audioPlayer.prepareToPlay()
        audioPlayer.play()
        DispatchQueue.main.async {
            self.initAudioTimer()
            self.btnPlay.setImage(UIImage(named: Constants.kPNGPause), for: .normal)
        }
    }
    
    func pauseAudio() {
        audioPlayer.pause()
        DispatchQueue.main.async {
            self.audioTimer.invalidate()
            self.btnPlay.setImage(UIImage(named: Constants.kPNGPlay), for: .normal)
        }
    }
    
    func initAudioTimer() {
        audioTimer.invalidate()
        audioTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime(timer:)), userInfo: nil, repeats: true)
    }
    
    func updateTime(timer: Timer) {
        audioSlider.value = Float(audioPlayer.currentTime)
        lblElapsed.text = Helpers.calculateTimeInterval(seconds: Int(audioSlider.value))
        
        if !audioPlayer.isPlaying {
            pauseAudio()
        }
    }
    
    @IBAction func forward(_ sender: Any) {
        if !btnForward.isUserInteractionEnabled {
            return
        }
        
        audioIndex += 1
        loadMetaData()
        initAudioPlayer()
    }
    
    @IBAction func backward(_ sender: Any) {
        if !btnBackward.isUserInteractionEnabled {
            return
        }
        
        audioIndex -= 1
        loadMetaData()
        initAudioPlayer()
    }
    
    @IBAction func seekToTime(_ sender: Any) {
        audioSlider.value = Float((sender as! UISlider).value)
        audioPlayer.currentTime = TimeInterval(audioSlider.value)
        lblElapsed.text = Helpers.calculateTimeInterval(seconds: Int(audioSlider.value))
    }
    
    //MARK: Table View DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return audioMetadata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.kCELLMetadata, for: indexPath) as! MetadataCell
        let metadataItem = audioMetadata[indexPath.item]
        cell.lblKey.text = metadataItem.commonKey
        if metadataItem.commonKey == Constants.kMETAArtwork {
            cell.lblValue.text = ""
            cell.imgViewArtwork.isHidden = false
            cell.imgViewArtwork.image = UIImage(data: metadataItem.dataValue!)
        }
        else {
            cell.imgViewArtwork.isHidden = true
            cell.lblValue.text = metadataItem.stringValue
        }
        return cell
    }
}
