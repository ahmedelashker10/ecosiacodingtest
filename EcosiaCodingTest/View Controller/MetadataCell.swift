//
//  MetadataCell.swift
//  EcosiaCodingTest
//
//  Created by Ahmed Elashker on 3/29/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

class MetadataCell: UITableViewCell {

    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var imgViewArtwork: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
