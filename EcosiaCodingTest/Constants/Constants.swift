//
//  Constants.swift
//  EcosiaCodingTest
//
//  Created by Ahmed Elashker on 3/29/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    static let kMETAArtwork = "artwork"
    
    //MARK: Types
    static let kTypeMP3 = "mp3"
    
    //MARK: Tracks
    static let kMP3CommDemo5 = "Commercial DEMO - 05"
    static let kMP3CommDemo7 = "Commercial DEMO - 07"
    static let kMP3CommDemo12 = "Commercial DEMO - 12"
    static let kMP3Grimey = "GrimeyPatches"
    static let kMP3Triumph = "The Triumph Of Obvious"
    
    //MARK: PNGs
    static let kPNGPlay = "play"
    static let kPNGPause = "pause"
    
    //MARK: Cell Reuse IDs
    static let kCELLMetadata = "MetadataCell"
}
