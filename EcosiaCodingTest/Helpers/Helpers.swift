//
//  Helpers.swift
//  EcosiaCodingTest
//
//  Created by Ahmed Elashker on 3/29/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

class Helpers: NSObject {
    static func calculateTimeInterval(seconds : Int) -> String {
        var totalTime = ""
        totalTime = "\(seconds)"
        let (h, m, s) = secondsToHoursMinutesSeconds (seconds: seconds)
        var hours = ""
        var munutes = ""
        var sec = ""
        
        if h < 10 {
            hours = "0\(h)"
        }else{
            hours = "\(h)"
        }
        
        if m < 10 {
            munutes = "0\(m)"
        }else{
            munutes = "\(m)"
        }
        
        if s < 10 {
            sec = "0\(s)"
        }else{
            sec = "\(s)"
        }
        
        if h == 0 {
            totalTime = munutes + ":" + sec
        }else{
            totalTime = hours + ":" + munutes + ":" + sec
        }
        print ("\(h) Hours, \(m) Minutes, \(s) Seconds")        
        
        return totalTime
    }
    
    private static func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
